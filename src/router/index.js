import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Track from "@/components/Track"
import Detail from "@/components/Detail"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/tracks',
      component: Track
    },
    {
      path:'/track/:id',
      props: true,
      component: Detail
    }
  ]
})
